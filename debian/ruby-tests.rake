require 'gem2deb/rake/spectask'

ENV['LANG'] = 'C.UTF-8'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end
